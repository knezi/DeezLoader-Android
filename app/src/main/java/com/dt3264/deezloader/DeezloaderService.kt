package com.dt3264.deezloader

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.HandlerThread
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.socket.client.IO
import java.io.File
import java.net.URISyntaxException
import java.util.*

class DeezloaderService : Service() {
    internal lateinit var context: Context
    private var serverThread: Thread? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            Objects.requireNonNull(intent.action)
            getString(R.string.serviceName)
        }
        return START_NOT_STICKY
    }

    // In case the service is deleted or crashes some how
    override fun onDestroy() {
        stopMyService()
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        // Used only in case of bound services.
        return null
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        val thread = HandlerThread("Thread Name")
        //Start the thread//
        thread.start()
        prepareServiceObserver()
        createNotificationChannel()
        prepareNodeServerListeners()
        startService()
    }


    // Notification methods
    private fun startService() {
        if (isServiceRunning) return

        val serviceIcon = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)

        val serverNotifIntent = Intent(applicationContext, BrowserActivity::class.java)
                .setAction(Intent.ACTION_MAIN)
                .addCategory(Intent.CATEGORY_LAUNCHER)
                .addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP)

        val serverPendingIntent = PendingIntent.getActivity(this, 0, serverNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(this, channelID)
                .setContentTitle(resources.getString(R.string.app_name))
                .setTicker(resources.getString(R.string.app_name))
                .setContentText(resources.getString(R.string.notificationService))
                .setSmallIcon(R.mipmap.ic_notification)
                .setLargeIcon(Bitmap.createScaledBitmap(serviceIcon, 128, 128, false))
                .setOngoing(false)
                .setContentIntent(serverPendingIntent)

        startServer()
        startForeground(serviceNotificationID, notification.build())
    }

    private fun stopMyService() {
        if (serverThread != null)
            serverThread!!.interrupt()
        serverThread = null
        stopForeground(true)
        stopSelf()
        isServiceRunning = false
    }

    private fun startServer() {
        try {
            serverThread = Thread {
                //The path where we expect the node project to be at runtime.
                val nodeDir = applicationContext.filesDir.absolutePath + "/deezerLoader"
                NodeNative.startNodeServer(arrayOf("node", "$nodeDir/app.js"))
            }
            serverThread!!.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Service notifications"
            val description = "Channel when Deezloader server is running"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelID, name, importance)
            channel.description = description
            channel.setSound(null, null)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService<NotificationManager>(NotificationManager::class.java)
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel)
        }
    }

    /*
    private fun notifyDownload(progress: Int, songName: String) {
        val mBuilder = NotificationCompat.Builder(baseContext, channelID)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle("Downloading: $songName")
                .setSubText(if (progress > 0) "$progress%" else "")
                .setProgress(100, progress, progress == 0)
                .setTimeoutAfter(60000)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        mBuilder.setOnlyAlertOnce(true)

        val notificationManager = NotificationManagerCompat.from(this)
        // notificationId is a unique int for each notification that you must define
        if (progress < 100) notificationManager.notify(notificationID, mBuilder.build())
    }

    private fun endNotification(songName: String, lastStatus: SongLastStatus) {
        val message = when (lastStatus) {
            SongLastStatus.READY -> "Downloaded: $songName"
            SongLastStatus.CANCELLED -> "Cancelled: $songName"

            SongLastStatus.ALREADY_DOWNLOADED -> "Already downloaded: $songName"
        }
        val mBuilder = NotificationCompat.Builder(baseContext, channelID)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle(message)
                .setPriority(NotificationCompat.PRIORITY_LOW)
        mBuilder.setOnlyAlertOnce(true)
        NotificationManagerCompat.from(this).notify(notificationID, mBuilder.build())
        notificationID++
    }
    */

    private fun prepareNodeServerListeners() {
        try {
            NodeNative.serviceSocket = IO.socket(serverURL)
        } catch (ignored: URISyntaxException) {}

        NodeNative.serviceSocket.on("siteReady") {
            transmitMessage(Message(1, getString(R.string.serverReady)))
            isServiceRunning = true
        }

        NodeNative.serviceSocket.on("openLink") { args ->
            val url = args[0] as String
            val i = Intent(Intent.ACTION_VIEW)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        /*
        serviceSocket.on("progressData") { args ->
            var progress: Int? = 0
            val songName: String = args[1] as String
            try {
                progress = args[0] as Int
            } catch (ignored: NullPointerException) {
            }

            notifyDownload(progress!!, songName)
        }

        serviceSocket.on("downloadCancelled") { args ->
            val songName = args[0] as String
            endNotification(songName, SongLastStatus.CANCELLED)
        }

        serviceSocket.on("downloadAlreadyExists") { args ->
            val songName = args[0] as String
            endNotification(songName, SongLastStatus.ALREADY_DOWNLOADED)
        }
        */

        NodeNative.serviceSocket.on("downloadReady") { args ->
            scanNewSongInternal(Uri.fromFile(File(args[0] as String)))
        }

        NodeNative.serviceSocket.connect()
    }

    private fun prepareServiceObserver() {
        NodeNative.serviceObserver = object : Observer<Message> {
            override fun onSubscribe(d: Disposable) {}

            override fun onNext(msg: Message) {
                Log.d("asd", msg.message)
            }

            override fun onError(e: Throwable) {}

            override fun onComplete() {}
        }
    }

    private fun transmitMessage(msg: Message) {
        Message.transmitMessage(msg).apply {
            // Run on a background thread
            subscribeOn(Schedulers.io())
            // Be notified on the main thread
            observeOn(AndroidSchedulers.mainThread())
            // The method that handles the data
            subscribe(NodeNative.mainObserver)
        }
    }

    // Function to add new songs to the mediastore once fully downloaded
    private fun scanNewSongInternal(fileUri: Uri) {
        sendBroadcast(
            Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).apply {
                data = fileUri
            }
        )
    }
}
